import json
import pytest



@pytest.mark.usefixtures("testapp")
class TestURLs:

    def test_home(self, testapp):
        getHome = testapp.get('/geschaeftsfuehrer/', follow_redirects=True)
        assert getHome.status_code == 200

    def test_neuVerkaufsstelle(self, testapp):
        getNeuV = testapp.get('/geschaeftsfuehrer/neuVerStl', follow_redirects=True)
        assert getNeuV.status_code == 200

        eintrag = dict([('name', 'Allgold-München'), ('typ', 'Geschäft'),
                        ('ort', 'München'), ('plz', '55555'),
                        ('str', 'Müller str.'), ('hnr', '5'), ('beschr', 'testest')])
        addPost = testapp.post('/geschaeftsfuehrer/neuVerStl', data=eintrag,
                                follow_redirects=True)

        assert addPost.status_code == 200
