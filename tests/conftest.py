from flask import Flask
from database import db, Verkaufstelle, Adresse, Inventar, Produkt, Lieferung, Elements, Verkauf
from factory import LoadBlueprints
import pytest

@pytest.fixture()
def testapp():
    tapp = Flask('testapp')
    tapp.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///testDB'
    tapp.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    with tapp.app_context():
        db.init_app(tapp)
        db.create_all()

    LoadBlueprints(tapp)

    client = tapp.test_client()
    return client