import pytest

@pytest.mark.usefixtures("testapp")
class TestURLs:

    def test_index(self, testapp):
        get1 = testapp.get('/', follow_redirects=False)
        assert get1.status_code == 200